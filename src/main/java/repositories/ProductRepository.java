package repositories;

import domain.LoginData;
import domain.Product;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IProductRepository;


import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ProductRepository implements IProductRepository {

    private final IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(Product entity) {
        try {
            String sql = "INSERT INTO product(product_name, price, description) " +
                    "VALUES(?, ?, ?, ?, ?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getProduct_name());
            stmt.setDouble(2, entity.getPrice());
            stmt.setString(3, entity.getDescription());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void update(Product entity) {
        String sql = "UPDATE product SET ";

        if (entity.getProduct_name() != null)
            sql += "product_name=?,";
        if (entity.getPrice() > 0)
            sql += "price=?,";
        if (entity.getDescription() != null)
            sql += "country=?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE product_id=?";

        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getProduct_name() != null)
                stmt.setString(i++, entity.getProduct_name());
            if (entity.getPrice() > 0)
                stmt.setDouble(i++, entity.getPrice());
            if (entity.getDescription() != null)
                stmt.setString(i++, entity.getDescription());
            stmt.setLong(i++, entity.getProduct_id());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void remove(Product entity) {

    }

    @Override
    public List<Product> query(String sql) {
        return null;
    }

    @Override
    public Product queryOne(String sql) {

        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                Product product = new Product(
                        rs.getLong("product_id"),
                        rs.getString("product_name"),
                        rs.getDouble("price"),
                        rs.getString("description")
                );
                return product;
            }
        } catch (SQLException throwables) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public Product getProductById(long product_id) {
        String sql = "SELECT * FROM product WHERE product_id = " + product_id + " LIMIT 1";
        return queryOne(sql);
    }


}
