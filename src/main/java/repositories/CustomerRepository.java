package repositories;

import domain.Customer;
import domain.LoginData;
import domain.Person;
import repositories.interfaces.ICustomerRepository;
import repositories.interfaces.IDBRepository;


import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class CustomerRepository implements ICustomerRepository {

    private final IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(Customer entity) {
        try {
            String sql = "INSERT INTO customer(username, password, job_title) " +
                    "VALUES(?, ?, ?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getUsername());
            stmt.setString(2, entity.getPassword());
            stmt.setString(3, entity.getJob_title());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }

    }

    @Override
    public void update(Customer entity) {
        String sql = "UPDATE users SET ";

        if (entity.getPassword() != null)
            sql += "password=?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE username=?";

        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getPassword() != null)
                stmt.setString(i++, entity.getPassword());
            stmt.setString(i++, entity.getUsername());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }

    }

    @Override
    public void remove(Customer entity) {

    }

    @Override
    public List<Customer> query(String sql) {
        return null;
    }

    @Override
    public Customer queryOne(String sql) {

        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                Customer customer = new Customer(
                        rs.getLong("customer_id"),
                        rs.getString("username"),
                        rs.getString("job_title")
                );
                return customer;
            }
        } catch (SQLException throwables) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public Customer getCustomerById(long customer_id) {
        String sql = "SELECT * FROM customer WHERE customer_id = " + customer_id + " LIMIT 1";
        return queryOne(sql);
    }

    @Override
    public Customer getCustomerByLogin(LoginData data) {
        try {
            String sql = "SELECT * FROM customer WHERE username = ? AND password = ?";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, data.getUsername());
            stmt.setString(2, data.getPassword());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Customer(
                        rs.getLong("customer_id"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("job_title")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

}
