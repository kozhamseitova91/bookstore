package repositories;

import domain.LoginData;
import domain.OrderStatus;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IOrderStatusRepository;


import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class OrderStatusRepository implements IOrderStatusRepository {

    private final IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(OrderStatus entity) {
        try {
            String sql = "INSERT INTO orderstatus(orderStatus) " +
                    "VALUES(?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getOrderStatus());


            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void update(OrderStatus entity) {
        String sql = "UPDATE orderstatus SET ";

        if (entity.getOrderStatus() != null)
            sql += "orderstatus =?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE order_id=?";

        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getOrderStatus() != null)
                stmt.setString(i++, entity.getOrderStatus());
            stmt.setLong(i++, entity.getOrder_id());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void remove(OrderStatus entity) {

    }

    @Override
    public List<OrderStatus> query(String sql) {
        return null;
    }

    @Override
    public OrderStatus queryOne(String sql) {

        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                OrderStatus orderStatus = new OrderStatus(
                        rs.getLong("order_id"),
                        rs.getString("orderStatus")
                );
                return orderStatus;
            }
        } catch (SQLException throwables) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public OrderStatus getOrderStatusById(long order_id) {
        String sql = "SELECT * FROM orderstatus WHERE order_id = " + order_id + " LIMIT 1";
        return queryOne(sql);
    }



}
