package repositories;

import domain.LoginData;
import domain.Person;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IPersonRepository;


import javax.ws.rs.BadRequestException;
import java.beans.PersistenceDelegate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class PersonRepository implements IPersonRepository {

    private final IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(Person entity) {
        try {
            String sql = "INSERT INTO users(name, surname, username, password, job_title) " +
                    "VALUES(?, ?, ?, ?, ?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getName());
            stmt.setString(2, entity.getSurname());
            stmt.setString(3, entity.getUsername());
            stmt.setString(4, entity.getPassword());
            stmt.setString(5, entity.getJob_title());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void update(Person entity) {
        String sql = "UPDATE users SET ";

        if (entity.getName() != null)
            sql += "name=?,";
        if (entity.getSurname() != null)
            sql += "surname=?,";
        if (entity.getPassword() != null)
            sql += "password=?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE username=?";

        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getName() != null)
                stmt.setString(i++, entity.getName());
            if (entity.getSurname() != null)
                stmt.setString(i++, entity.getSurname());
            if (entity.getPassword() != null)
                stmt.setString(i++, entity.getPassword());
            stmt.setString(i++, entity.getUsername());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void remove(Person entity) {

    }

    @Override
    public List<Person> query(String sql) {
        return null;
    }

    @Override
    public Person queryOne(String sql) {

        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                Person person = new Person(
                        rs.getLong("person_id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("job_title")
                );
                return person;
            }
        } catch (SQLException throwables) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public Person getPersonById(long person_id) {
        String sql = "SELECT * FROM person WHERE person_id = " + person_id + " LIMIT 1";
        return queryOne(sql);
    }

    @Override
    public Person getPersonByLogin(LoginData data) {
        try {
            String sql = "SELECT * FROM person WHERE username = ? AND password = ?";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, data.getUsername());
            stmt.setString(2, data.getPassword());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Person(
                        rs.getLong("person_id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("job_title")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }


}
