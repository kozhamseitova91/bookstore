package repositories;

import domain.Bill;
import domain.LoginData;
import repositories.interfaces.IBillRepository;
import repositories.interfaces.IDBRepository;

import javax.ws.rs.BadRequestException;
import java.sql.*;
import java.util.List;

public class BillRepository implements IBillRepository {
    private final IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(Bill entity) {
        try {
            String sql = "INSERT INTO bill( order_id, issuedate, duedate, sum) " +
                    "VALUES(?, ?, ?, ?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, entity.getOrder_id());
            stmt.setDate(2, (Date) entity.getIssueDate());
            stmt.setDate(3, (Date) entity.getDueDate());
            stmt.setDouble(4, entity.getSum());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void update(Bill entity) {
        String sql = "UPDATE bill SET ";

        if (entity.getIssueDate() != null)
            sql += "issuedate=?,";
        if (entity.getDueDate() != null)
            sql += "duedate=?,";
        if (entity.getSum() > 0)
            sql += "sum=?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE order_id=?";

        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getIssueDate() != null)
                stmt.setDate(i++, (Date) entity.getIssueDate());
            if (entity.getDueDate() != null)
                stmt.setDate(i++, (Date) entity.getDueDate());
            if (entity.getSum() > 0)
                stmt.setDouble(i++, entity.getSum());
            stmt.setLong(i++, entity.getOrder_id());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void remove(Bill entity) {

    }

    @Override
    public List<Bill> query(String sql) {
        return null;
    }

    @Override
    public Bill queryOne(String sql) {

        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                Bill bill = new Bill(
                        rs.getLong("order_id"),
                        rs.getDate("issueDate"),
                        rs.getDate("dueDate"),
                        rs.getDouble("sum")
                );
                return bill;
            }
        } catch (SQLException throwables) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public Bill getBillById(long order_id) {
        String sql = "SELECT * FROM bill WHERE order_id = " + order_id + " LIMIT 1";
        return queryOne(sql);
    }




}
