package repositories.interfaces;

import domain.Address;
import domain.LoginData;

public interface IAddressRepository extends IEntityRepository<Address>{
    Address getAddressById(long customer_id);
}
