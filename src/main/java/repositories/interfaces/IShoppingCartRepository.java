package repositories.interfaces;

import domain.LoginData;
import domain.ShoppingCart;

public interface IShoppingCartRepository extends IEntityRepository<ShoppingCart>{
    ShoppingCart getShoppingCartById(long customer_id);
}
