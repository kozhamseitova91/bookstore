package repositories.interfaces;

import domain.LoginData;
import domain.Person;

public interface IPersonRepository extends IEntityRepository<Person>{
    Person getPersonById(long person_id);

    Person getPersonByLogin(LoginData data);
}
