package repositories.interfaces;

import domain.Item;
import domain.LoginData;

public interface IItemRepository extends IEntityRepository<Item>{
    Item getItemById(long item_id);

}
