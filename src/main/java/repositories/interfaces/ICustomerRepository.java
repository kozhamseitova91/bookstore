package repositories.interfaces;

import domain.Customer;
import domain.LoginData;
import domain.Person;

public interface ICustomerRepository extends IEntityRepository<Customer>{
    Customer getCustomerById(long customer_id);

    Customer getCustomerByLogin(LoginData data);
}
