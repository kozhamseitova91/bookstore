package repositories.interfaces;

import domain.Bill;
import domain.LoginData;

public interface IBillRepository extends IEntityRepository<Bill>{
    Bill getBillById(long order_id);
}
