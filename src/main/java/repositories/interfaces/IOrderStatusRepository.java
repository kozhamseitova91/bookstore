package repositories.interfaces;

import domain.LoginData;
import domain.OrderStatus;

public interface IOrderStatusRepository extends IEntityRepository<OrderStatus>{
    OrderStatus getOrderStatusById(long order_id);

}
