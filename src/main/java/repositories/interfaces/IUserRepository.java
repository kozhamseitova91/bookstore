package repositories.interfaces;

import domain.User;

public interface IUserRepository {

    User getUserByUsername(String username);

}
