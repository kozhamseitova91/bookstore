package repositories.interfaces;

import domain.LoginData;
import domain.Order;

public interface IOrderRepository extends IEntityRepository<Order>{
    Order getOrderById(long order_id);
}
