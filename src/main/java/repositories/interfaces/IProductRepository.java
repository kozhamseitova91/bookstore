package repositories.interfaces;

import domain.LoginData;
import domain.Product;

public interface IProductRepository extends IEntityRepository<Product>{
    Product getProductById(long product_id);
}
