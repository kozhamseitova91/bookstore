package repositories;

import domain.LoginData;
import domain.ShoppingCart;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IShoppingCartRepository;


import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ShoppingCartRepository implements IShoppingCartRepository {

    private final IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(ShoppingCart entity) {
        try {
            String sql = "INSERT INTO shoppingcart(status) " +
                    "VALUES(?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getStatus());


            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void update(ShoppingCart entity) {
        String sql = "UPDATE status SET ";

        if (entity.getStatus() != null)
            sql += "status =?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE customer_id=?";

        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getStatus() != null)
                stmt.setString(i++, entity.getStatus());
            stmt.setLong(i++, entity.getCustomer_id());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void remove(ShoppingCart entity) {

    }

    @Override
    public List<ShoppingCart> query(String sql) {
        return null;
    }

    @Override
    public ShoppingCart queryOne(String sql) {

        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                ShoppingCart shoppingCart = new ShoppingCart(
                        rs.getLong("customer_id"),
                        rs.getString("status")
                );
                return shoppingCart;
            }
        } catch (SQLException throwables) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public ShoppingCart getShoppingCartById(long customer_id) {
        String sql = "SELECT * FROM shoppingcart WHERE customer_id = " + customer_id + " LIMIT 1";
        return queryOne(sql);
    }


}
