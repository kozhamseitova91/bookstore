package repositories;

import domain.LoginData;
import domain.Order;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IOrderRepository;


import javax.ws.rs.BadRequestException;
import java.sql.*;
import java.util.List;

public class OrderRepository implements IOrderRepository {

    private final IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(Order entity) {
        try {
            String sql = "INSERT INTO orderb(orderdate, sum) " +
                    "VALUES(?, ?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setDate(1, (Date) entity.getOrderDate());
            stmt.setDouble(3, entity.getSum());


            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void update(Order entity) {
        String sql = "UPDATE order SET ";

        if (entity.getOrderDate() != null)
            sql += "orderdate=?,";
        if (entity.getSum() > 0)
            sql += "sum=?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE order_id=?";

        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getOrderDate() != null)
                stmt.setDate(i++, (Date) entity.getOrderDate());
            if (entity.getSum() > 0)
                stmt.setDouble(i++, entity.getSum());
            stmt.setLong(i++, entity.getOrder_id());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void remove(Order entity) {

    }

    @Override
    public List<Order> query(String sql) {
        return null;
    }

    @Override
    public Order queryOne(String sql) {

        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                Order order = new Order(
                        rs.getLong("order_id"),
                        rs.getDate("orderDate"),
                        rs.getDouble("sum")
                );
                return order;
            }
        } catch (SQLException throwables) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public Order getOrderById(long order_id) {
        String sql = "SELECT * FROM orders WHERE order_id = " + order_id + " LIMIT 1";
        return queryOne(sql);
    }


}
