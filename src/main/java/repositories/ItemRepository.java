package repositories;

import domain.Item;
import domain.LoginData;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IItemRepository;


import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ItemRepository implements IItemRepository {

    private final IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(Item entity) {
        try {
            String sql = "INSERT INTO address(quantity, price) " +
                    "VALUES(?, ?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setInt(1, entity.getQuantity());
            stmt.setDouble(2, entity.getPrice());


            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void update(Item entity) {
        String sql = "UPDATE address SET ";


        if (entity.getQuantity() > 0)
            sql += "quantity=?,";
        if (entity.getPrice() > 0)
            sql += "price=?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE item_id=?";

        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getQuantity() > 0)
                stmt.setInt(i++, entity.getQuantity());
            if (entity.getPrice() > 0)
                stmt.setDouble(i++, entity.getPrice());
            stmt.setLong(i++, entity.getItem_id());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }


    @Override
    public void remove(Item entity) {

    }

    @Override
    public List<Item> query(String sql) {
        return null;
    }

    @Override
    public Item queryOne(String sql) {

        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                Item item = new Item(
                        rs.getLong("item_id"),
                        rs.getInt("quantity"),
                        rs.getDouble("price")
                );
                return item;
            }
        } catch (SQLException throwables) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public Item getItemById(long item_id) {
        String sql = "SELECT * FROM item WHERE item_id = " + item_id + " LIMIT 1";
        return queryOne(sql);
    }


}
