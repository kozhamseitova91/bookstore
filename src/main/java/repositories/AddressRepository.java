package repositories;

import domain.Address;
import domain.LoginData;
import domain.Person;
import repositories.interfaces.IAddressRepository;
import repositories.interfaces.IDBRepository;


import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class AddressRepository implements IAddressRepository {

    private final IDBRepository dbRepo = new PostgresRepository();

    @Override
    public void add(Address entity) {
        try {
            String sql = "INSERT INTO address(street, city, country, phone, zipcode) " +
                    "VALUES(?, ?, ?, ?, ?)";
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getStreet());
            stmt.setString(2, entity.getCity());
            stmt.setString(3, entity.getCountry());
            stmt.setString(4, entity.getPhone());
            stmt.setInt(5, entity.getZipcode());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void update(Address entity) {
        String sql = "UPDATE address SET ";

        if (entity.getStreet() != null)
            sql += "street=?,";
        if (entity.getCity() != null)
            sql += "city=?,";
        if (entity.getCountry() != null)
            sql += "country=?,";
        if (entity.getPhone() != null)
            sql += "phone=?,";
        if (entity.getZipcode() > 0)
            sql += "zipcode=?,";

        sql = sql.substring(0, sql.length() - 1);
        sql += " WHERE customer_id=?";

        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            int i = 1;
            if (entity.getStreet() != null)
                stmt.setString(i++, entity.getStreet());
            if (entity.getCity() != null)
                stmt.setString(i++, entity.getCity());
            if (entity.getCountry() != null)
                stmt.setString(i++, entity.getCountry());
            if (entity.getPhone() != null)
                stmt.setString(i++, entity.getPhone());
            if (entity.getZipcode() > 0)
                stmt.setInt(i++, entity.getZipcode());
            stmt.setLong(i++, entity.getCustomer_id());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }


    }

    @Override
    public void remove(Address entity) {


    }

    @Override
    public List<Address> query(String sql) {
        return null;
    }

    @Override
    public Address queryOne(String sql) {

        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                Address address = new Address(
                        rs.getLong("customer_id"),
                        rs.getString("street"),
                        rs.getString("city"),
                        rs.getString("country"),
                        rs.getString("phone"),
                        rs.getInt("zipcode")
                );
                return address;
            }
        } catch (SQLException throwables) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public Address getAddressById(long customer_id) {
        String sql = "SELECT * FROM address WHERE customer_id = " + customer_id + " LIMIT 1";
        return queryOne(sql);
    }




}
