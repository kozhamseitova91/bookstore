package domain;

import java.util.Date;

public class OrderStatus{
    private long order_id;
    private String orderStatus;

    public OrderStatus(long order_id, String orderStatus) {
        this.order_id = order_id;
        this.orderStatus = orderStatus;
    }

    public long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long order_id) {
        this.order_id = order_id;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return "OrderStatus{" +
                "order_id=" + order_id +
                ", orderStatus='" + orderStatus + '\'' +
                '}';
    }
}
