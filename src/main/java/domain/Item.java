package domain;


public class Item{
    private long item_id;
    private int quantity;
    private double price;

    public Item(long item_id, int quantity, double price){
        this.item_id = item_id;
        this.quantity = quantity;
        this.price = price;
    }

    public long getItem_id() {
        return item_id;
    }

    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "item_id=" + item_id +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
