package domain;

import java.util.Date;

public class Bill {
    private long order_id;
    private Date issueDate;
    private Date dueDate;
    private Double sum;

    public Bill(long order_id, Date issueDate, Date dueDate, Double sum) {
        this.order_id = order_id;
        this.issueDate = issueDate;
        this.dueDate = dueDate;
        this.sum = sum;
    }

    public long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long order_id) {
        this.order_id = order_id;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "order_id=" + order_id +
                ", issueDate=" + issueDate +
                ", dueDate=" + dueDate +
                ", sum=" + sum +
                '}';
    }
}
