package domain;


public class Customer extends User{
    private long customer_id;

    public Customer(long customer_id, String username, String password, String job_title) {
        super(username, password, job_title);
        this.customer_id = customer_id;
    }

    public Customer(long customer_id, String username, String job_title) {
        super(username, job_title);
        this.customer_id = customer_id;
    }

    public long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(long customer_id) {
        this.customer_id = customer_id;
    }

    @Override
    public String toString() {
        return super.username;
    }
}
