package domain;


public class User {
    protected String username;
    private String password;
    protected String job_title;

    public User() {}

    public User(String username, String password, String job_title) {
        this.username = username;
        this.password = password;
        this.job_title = job_title;
    }

    public User(String username, String job_title) {
        this.username = username;
        this.job_title = job_title;
    }



    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) { this.password = password; }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    @Override
    public String toString() {
        return username;
    }
}
