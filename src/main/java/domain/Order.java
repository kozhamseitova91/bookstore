package domain;


import java.util.Date;

public class Order{
    private long order_id;
    private Date orderDate;
    private Double sum;

    public Order(long order_id, Date orderDate, Double sum) {
        this.order_id = order_id;
        this.orderDate = orderDate;
        this.sum = sum;
    }

    public long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long order_id) {
        this.order_id = order_id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "Order{" +
                "order_id=" + order_id +
                ", orderDate=" + orderDate +
                ", sum=" + sum +
                '}';
    }
}
