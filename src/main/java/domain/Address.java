package domain;


public class Address {

    private long customer_id;
    private String street;
    private String city;
    private String country;
    private String phone;
    private int zipcode;


    public Address(long customer_id, String street, String city, String country, String phone, int zipcode) {
        this.customer_id = customer_id;
        this.street = street;
        this.city = city;
        this.country = country;
        this.phone = phone;
        this.zipcode = zipcode;
    }

    public long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(long customer_id) {
        this.customer_id = customer_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "customer_id=" + customer_id +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", phone='" + phone + '\'' +
                ", zipcode=" + zipcode +
                '}';
    }


}
