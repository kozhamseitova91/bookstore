package domain;


public class ShoppingCart{
    private long customer_id;
    private String status;

    public ShoppingCart(long customer_id, String status){
        this.customer_id = customer_id;
        this.status = status;
    }

    public long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(long customer_id) {
        this.customer_id = customer_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "customer_id=" + customer_id +
                ", status='" + status + '\'' +
                '}';
    }
}
