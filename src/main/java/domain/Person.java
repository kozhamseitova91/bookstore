package domain;


public class Person extends User{
    private long person_id;
    private String name;
    private String surname;

    public Person(long person_id, String name, String surname, String username, String password, String job_title) {
        super(username, password, job_title);
        this.person_id = person_id;
        this.name = name;
        this.surname = surname;
    }

    public Person(long person_id, String name, String surname, String username, String job_title) {
       super(username, job_title);
        this.person_id = person_id;
        this.name = name;
        this.surname = surname;
    }

    public long getPerson_id() {
        return person_id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setPerson_id(long person_id) {
        this.person_id = person_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    @Override
    public String toString() {
        return name + " " + surname;
    }
}
