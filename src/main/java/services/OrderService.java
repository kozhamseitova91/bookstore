package services;


import domain.Order;
import repositories.OrderRepository;
import repositories.interfaces.IOrderRepository;
import services.interfaces.IOrderService;

public class OrderService implements IOrderService {
    private final IOrderRepository orderRepo = new OrderRepository();

    @Override
    public Order getOrderById(long order_id) {
        return orderRepo.getOrderById(order_id);
    }
}
