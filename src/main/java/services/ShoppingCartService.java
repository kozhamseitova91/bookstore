package services;


import domain.ShoppingCart;
import repositories.ShoppingCartRepository;
import repositories.interfaces.IShoppingCartRepository;
import services.interfaces.IShoppingCartService;

public class ShoppingCartService implements IShoppingCartService {
    private final IShoppingCartRepository shoppingCartRepo = new ShoppingCartRepository();

    @Override
    public ShoppingCart getShoppingCartById(long customer_id) {
        return shoppingCartRepo.getShoppingCartById(customer_id);
    }
}
