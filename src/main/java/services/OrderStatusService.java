package services;


import domain.OrderStatus;
import repositories.OrderStatusRepository;
import repositories.interfaces.IOrderStatusRepository;
import services.interfaces.IOrderStatusService;

public class OrderStatusService implements IOrderStatusService {
    private final IOrderStatusRepository orderStatusRepo = new OrderStatusRepository();

    @Override
    public OrderStatus getOrderStatusById(long order_id) {
        return orderStatusRepo.getOrderStatusById(order_id);
    }
}
