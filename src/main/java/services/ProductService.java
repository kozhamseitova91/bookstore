package services;

import domain.Product;
import repositories.ProductRepository;
import repositories.interfaces.IProductRepository;
import services.interfaces.IProductService;

public class ProductService implements IProductService {
    private final IProductRepository productRepo = new ProductRepository();

    @Override
    public Product getProductById(long product_id) {
        return productRepo.getProductById(product_id);
    }
}
