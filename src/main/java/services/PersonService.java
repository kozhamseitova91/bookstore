package services;

import domain.Person;
import repositories.PersonRepository;
import repositories.interfaces.IPersonRepository;
import services.interfaces.IPersonService;

public class PersonService implements IPersonService {
    private final IPersonRepository personRepo = new PersonRepository();

    @Override
    public Person getPersonById(long person_id) {
        return personRepo.getPersonById(person_id);
    }
}
