package services;

import domain.*;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import repositories.CustomerRepository;
import repositories.PersonRepository;
import repositories.UserRepository;
import repositories.interfaces.ICustomerRepository;
import repositories.interfaces.IPersonRepository;
import repositories.interfaces.IUserRepository;
import services.interfaces.IAuthorizationService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Random;

public class AuthorizationService implements IAuthorizationService {

    private final IPersonRepository personRepo = new PersonRepository();
    private final ICustomerRepository customerRepo = new CustomerRepository();
    private final IUserRepository userRepo = new UserRepository();


    @Override
    public AccessToken authenticate(LoginData data) throws Exception {
        User user = signIn(data);
        AccessToken token = new AccessToken(getToken(user));
        return token;
    }

    @Override
    public User getUserByUsername(String issuer) {
        return userRepo.getUserByUsername(issuer);
    }

    private User signIn(LoginData data) throws Exception {
        User person = personRepo.getPersonByLogin(data);
        User customer = customerRepo.getCustomerByLogin(data);
        if(customer != null){
            return customer;
        }
        else if(person != null){
            return person;
        }
        else{
            throw new Exception("User doesn't exist!");
        }
    }


    private String getToken(User user){
        Instant now = Instant.now();
        String secretWord = "TheStrongestSecretKeyICanThinkOf";
        return Jwts.builder()
                .setIssuer(user.getUsername())
                .setIssuedAt(Date.from(now))
                .claim("1d20", new Random().nextInt(20) + 1)
                .setExpiration(Date.from(now.plus(10, ChronoUnit.MINUTES)))
                .signWith(Keys.hmacShaKeyFor(secretWord.getBytes()))
                .compact();

    }


}
