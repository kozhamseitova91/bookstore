package services;

import domain.Item;
import repositories.ItemRepository;
import repositories.interfaces.IItemRepository;
import services.interfaces.IItemService;

public class ItemService implements IItemService {
    private final IItemRepository itemRepo = new ItemRepository();

    @Override
    public Item getItemById(long item_id) {
        return itemRepo.getItemById(item_id);
    }
}
