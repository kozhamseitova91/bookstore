package services;

import domain.Customer;
import repositories.CustomerRepository;
import repositories.interfaces.ICustomerRepository;
import services.interfaces.ICustomerService;

public class CustomerService implements ICustomerService {
    private final ICustomerRepository customerRepo = new CustomerRepository();

    @Override
    public Customer getCustomerById(long customer_id) {
        return customerRepo.getCustomerById(customer_id);
    }
}