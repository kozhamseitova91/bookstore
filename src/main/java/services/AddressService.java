package services;


import domain.Address;
import repositories.AddressRepository;
import repositories.interfaces.IAddressRepository;
import services.interfaces.IAddressService;

public class AddressService implements IAddressService {
    private final IAddressRepository addressRepo = new AddressRepository();

    @Override
    public Address getAddressById(long customer_id) {
        return addressRepo.getAddressById(customer_id);
    }
}
