package services;

import domain.Bill;
import repositories.BillRepository;
import repositories.interfaces.IBillRepository;
import services.interfaces.IBillService;

public class BillService implements IBillService {
private final IBillRepository BillRepo = new BillRepository();

@Override
public Bill getBillById(long order_id) {
        return BillRepo.getBillById(order_id);
        }
}
