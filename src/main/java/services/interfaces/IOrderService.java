package services.interfaces;

import domain.Order;

public interface IOrderService {

    Order getOrderById(long order_id);
}
