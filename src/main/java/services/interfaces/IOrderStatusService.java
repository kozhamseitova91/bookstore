package services.interfaces;


import domain.OrderStatus;

public interface IOrderStatusService {
    OrderStatus getOrderStatusById(long order_id);
}
