package services.interfaces;


import domain.ShoppingCart;

public interface IShoppingCartService {


    ShoppingCart getShoppingCartById(long customer_id);
}
