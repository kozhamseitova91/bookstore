package services.interfaces;

import domain.Person;

public interface IPersonService {


    Person getPersonById(long person_id);
}
