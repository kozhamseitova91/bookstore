package services.interfaces;

import domain.Bill;

public interface IBillService {

    Bill getBillById(long order_id);
}
