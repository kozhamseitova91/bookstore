package services.interfaces;

import domain.Customer;

public interface ICustomerService {
    Customer getCustomerById(long customer_id);
}
