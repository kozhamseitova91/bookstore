package services.interfaces;

import domain.Product;

public interface IProductService {
    Product getProductById(long product_id);
}
