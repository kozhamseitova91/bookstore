package services.interfaces;


import domain.Address;

public interface IAddressService {


    Address getAddressById(long customer_id);
}
