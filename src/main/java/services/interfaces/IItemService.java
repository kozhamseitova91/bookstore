package services.interfaces;


import domain.Item;

public interface IItemService {

    Item getItemById(long item_id);
}
