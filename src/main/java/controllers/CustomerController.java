package controllers;

import domain.Customer;
import filters.сustomAnnotations.JwtTokenNeeded;
import services.CustomerService;
import services.interfaces.ICustomerService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/customer")
public class CustomerController {
    private final ICustomerService customerService = new CustomerService();

    @GET
    public String index(){
        return "Hello from User controller";
    }

    @JwtTokenNeeded
    @GET
    @Path("/{customer_id}")
    public Response getCustomerById(@PathParam("customer_id") long customer_id){
        Customer customer;
        try {
            customer = customerService.getCustomerById(customer_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This user cannot be created").build();
        }

        if (customer == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("User does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(customer)
                    .build();
        }

    }
}
