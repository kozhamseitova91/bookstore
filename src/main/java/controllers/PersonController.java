package controllers;

import domain.Person;
import filters.сustomAnnotations.JwtTokenNeeded;
import services.PersonService;
import services.interfaces.IPersonService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/person")
public class PersonController {
    private final IPersonService personService = new PersonService();

    @GET
    public String index(){
        return "Hello from Person controller";
    }

    @JwtTokenNeeded
    @GET
    @Path("/{person_id}")
    public Response getPersonById(@PathParam("person_id") long person_id){
        Person person;
        try {
            person = personService.getPersonById(person_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This user cannot be created").build();
        }

        if (person == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Person does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(person)
                    .build();
        }

    }
}
