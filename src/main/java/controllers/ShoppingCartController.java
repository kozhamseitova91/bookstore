package controllers;

import domain.ShoppingCart;
import filters.сustomAnnotations.JwtTokenNeeded;
import services.ShoppingCartService;
import services.interfaces.IShoppingCartService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/shoppingCart")
public class ShoppingCartController {
    private final IShoppingCartService shoppingCartService = new ShoppingCartService();

    @GET
    public String index(){
        return "Hello from ShoppingCart controller";
    }

    @JwtTokenNeeded
    @GET
    @Path("/{customer_id}")
    public Response getShoppingCartById(@PathParam("customer_id") long customer_id){
        ShoppingCart shoppingCart;
        try {
            shoppingCart = shoppingCartService.getShoppingCartById(customer_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This shoppingCart cannot be created").build();
        }

        if (shoppingCart == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("ShoppingCart does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(shoppingCart)
                    .build();
        }

    }
}
