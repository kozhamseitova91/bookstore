package controllers;

import domain.Product;
import filters.сustomAnnotations.JwtTokenNeeded;
import services.ProductService;
import services.interfaces.IProductService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/product")
public class ProductController {
    private final IProductService productService = new ProductService();

    @GET
    public String index(){
        return "Hello from Product controller";
    }

    @JwtTokenNeeded
    @GET
    @Path("/{product_id}")
    public Response getProductnById(@PathParam("product_id") long product_id){
        Product product;
        try {
            product = productService.getProductById(product_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This product cannot be created").build();
        }

        if (product == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Product does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(product)
                    .build();
        }

    }
}
