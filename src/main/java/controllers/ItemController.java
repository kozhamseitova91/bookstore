package controllers;

import domain.Item;
import filters.сustomAnnotations.JwtTokenNeeded;
import services.ItemService;
import services.interfaces.IItemService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/item")
public class ItemController {
    private final IItemService itemService = new ItemService();

    @GET
    public String index(){
        return "Hello from Item controller";
    }

    @JwtTokenNeeded
    @GET
    @Path("/{item_id}")
    public Response getItemById(@PathParam("item_id") long item_id){
        Item item;
        try {
            item = itemService.getItemById(item_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This item cannot be created").build();
        }

        if (item == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Item does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(item)
                    .build();
        }

    }
}
