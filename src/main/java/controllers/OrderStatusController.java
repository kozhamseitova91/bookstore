package controllers;

import domain.OrderStatus;
import filters.сustomAnnotations.JwtTokenNeeded;
import services.OrderStatusService;
import services.interfaces.IOrderStatusService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/orderStatus")
public class OrderStatusController {
    private final IOrderStatusService orderStatusService = new OrderStatusService();

    @GET
    public String index(){
        return "Hello from OrderStatus controller";
    }

    @JwtTokenNeeded
    @GET
    @Path("/{order_id}")
    public Response getOrderStatusById(@PathParam("order_id") long order_id){
        OrderStatus orderStatus;
        try {
            orderStatus = orderStatusService.getOrderStatusById(order_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This orderStatus cannot be created").build();
        }

        if (orderStatus == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("OrderStatus does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(orderStatus)
                    .build();
        }

    }
}
