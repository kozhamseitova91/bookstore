package controllers;

import domain.Bill;
import filters.сustomAnnotations.JwtTokenNeeded;
import services.BillService;
import services.interfaces.IBillService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/bill")
public class BillController {
    private final IBillService billService = new BillService();

    @GET
    public String index(){
        return "Hello from Bill controller";
    }

    @JwtTokenNeeded
    @GET
    @Path("/{order_id}")
    public Response getBillById(@PathParam("order_id") long order_id){
        Bill bill;
        try {
            bill = billService.getBillById(order_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This bill cannot be created").build();
        }

        if (bill == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Bill does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(bill)
                    .build();
        }

    }
}
