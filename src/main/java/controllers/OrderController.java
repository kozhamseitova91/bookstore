package controllers;

import domain.Order;
import filters.сustomAnnotations.JwtTokenNeeded;
import services.OrderService;
import services.interfaces.IOrderService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/order")
public class OrderController {
    private final IOrderService orderService = new OrderService();

    @GET
    public String index(){
        return "Hello from Order controller";
    }

    @JwtTokenNeeded
    @GET
    @Path("/{order_id}")
    public Response getOrderById(@PathParam("order_id") long order_id){
        Order order;
        try {
            order = orderService.getOrderById(order_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This order cannot be created").build();
        }

        if (order == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Order does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(order)
                    .build();
        }

    }
}
