package controllers;

import domain.Address;
import filters.сustomAnnotations.JwtTokenNeeded;
import services.AddressService;
import services.interfaces.IAddressService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/address")
public class AddressController {
    private final IAddressService addressService = new AddressService();

    @GET
    public String index(){
        return "Hello from Address controller";
    }

    @JwtTokenNeeded
    @GET
    @Path("/{customer_id}")
    public Response getAddressById(@PathParam("customer_id") long customer_id){
        Address address;
        try {
            address = addressService.getAddressById(customer_id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This user cannot be created").build();
        }

        if (address == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Address does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(address)
                    .build();
        }

    }
}
